#pragma once

#include "GLApplication.h"

#include "AssetLoading/AssetLoader.h"
#include "Resources/Asset.h"

namespace GLEngine
{
	class FlyCamera;
	class Mesh;
};

class Lighting : public GLEngine::GLApplication
{
public:
	Lighting(std::string str, unsigned int uiWidth, unsigned int uiHeight);
	~Lighting();

	bool Startup() override;

	void Shutdown() override;

	bool Update(double dt) override;

	void Render() override;

private:
    GLEngine::FlyCamera*	m_pFlyCamera;
	GLEngine::AssetLoader*	m_pLoader;

	bool m_bDrawGizmoGrid;

	GLEngine::Mesh* m_pSpear;

	glm::mat4 m_lightTransform;
};