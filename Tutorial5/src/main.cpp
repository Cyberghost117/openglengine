#include "GLApplication.h"

#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include "Lighting.h"

int main()
{
	Lighting app("Lighting Example", 1280, 720);
    
	app.Run();

    return 0;
}