#pragma once

#include "GLApplication.h"

#include "AssetLoading/AssetLoader.h"
#include "Resources/Asset.h"

namespace GLEngine
{
	class FlyCamera;
};

class Geometry : public GLEngine::GLApplication
{
public:
	Geometry(std::string str, unsigned int uiWidth, unsigned int uiHeight);
	~Geometry();

	bool Startup() override;

	void Shutdown() override;

	bool Update(double dt) override;

	void Render() override;

private:
    GLEngine::FlyCamera*	m_pFlyCamera;

	GLEngine::AssetLoader*	m_pLoader;
	GLEngine::Mesh*			m_pFBXBunnyMesh;
	GLEngine::Mesh*			m_pOBJBunnyMesh;

	GLEngine::Shader*		m_pShader;
	bool m_bDrawGizmoGrid;
};