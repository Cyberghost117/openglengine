#include "Logging/DebugConsole.h"
#include <algorithm>
#include <cctype>

#include "GLFW/glfw3.h"

DebugConsole* DebugConsole::ms_pInstance = nullptr;


DebugConsole::DebugConsole()
{
	m_currLevel = MessageLevel::General;
	m_autoPopupLevel = MessageLevel::Error;

	m_errorLevelsAsText = "General\0Warning\0Error\0Critical\0\0";
	m_currentErrorLevelLocation = 0;

	m_bFilteringEnabled = false;

	m_filterBuffer = new char[m_filterBufferSize];
	memset(m_filterBuffer, 0, sizeof(char) * m_filterBufferSize);

	HideDebugWindow();
}


DebugConsole::~DebugConsole()
{
	delete[] m_filterBuffer;
}

void DebugConsole::Create()
{
	assert(ms_pInstance == nullptr && "Debug Console already created");
	ms_pInstance = new DebugConsole();
}

void DebugConsole::Shutdown()
{
	delete ms_pInstance;
}

DebugConsole* DebugConsole::Get()
{
	assert(ms_pInstance != nullptr && "You need to call Create before you can 'get' the Debug Console");
	return ms_pInstance;
}

void DebugConsole::ShowDebugWindow()
{
	m_bDebugWindowVisible = true;
}

void DebugConsole::HideDebugWindow()
{
	m_bDebugWindowVisible = false;
}

void DebugConsole::Render()
{
	if (m_bDebugWindowVisible)
	{
		ImGui::Begin("Debug Console", &m_bDebugWindowVisible, ImGuiWindowFlags_NoFocusOnAppearing);

		RenderMessageLevelButtons();

		ImGui::Separator();

		ImGui::BeginChild("DebugTextArea");
		for (unsigned int i = 0; i < m_messageList.size(); ++i)
		{
			DisplayMessage(m_messageList[i]);
		}
		ImGui::EndChild();

		ImGui::End();
	}
}

void DebugConsole::PopulateWithDebugText()
{
	for (int i = 0; i < 100; ++i)
	{
		MessageInfo info;
		info.category = std::to_string(i % 5);
		info.level = (MessageLevel)(rand() % MessageLevel::All);
		info.string = "Some random debug text!";

		m_messageList.push_back(info);
	}
}

void DebugConsole::Message(MessageLevel level, std::string category, std::string msg)
{
	MessageInfo info;
	info.category = category;
	info.level = level;
	info.string = msg;
	info.time = glfwGetTime();
	m_messageList.push_back(info);

	if (info.level >= m_autoPopupLevel)
		ShowDebugWindow();
}

void DebugConsole::DisplayMessage(MessageInfo& msg)
{
	if (msg.level < m_currLevel) return;
	if (m_bFilteringEnabled && !m_filterText.empty() && !CheckForFilterMatch(msg)) return;

	ImVec4 col = GetColourBasedOnMessageLevel(msg.level);
	ImGui::TextColored(col, "%5f - %10s| %s", msg.time, msg.category.c_str(), msg.string.c_str());
}

ImVec4 DebugConsole::GetColourBasedOnMessageLevel(MessageLevel level)
{
	switch (level)
	{
	default:
		break;
	case General:
		return ImVec4(1, 1, 1, 1);
	case Warning:
		return ImVec4(1, 1, 0, 1);
	case Error:
		return ImVec4(1, 0, 0, 1);
	case Critical:
		return ImVec4(1, 0, 1, 1);
	}

	return ImVec4(1, 1, 1, 1);
}


void DebugConsole::RenderMessageLevelButtons()
{
	ImGui::Text("Min Message Level:");
	ImGui::SameLine();
	ImGui::Combo("", &m_currentErrorLevelLocation, m_errorLevelsAsText);
	ImGui::Checkbox("Enable Filter", &m_bFilteringEnabled);
	ImGui::SameLine();
	ImGui::InputText("", m_filterBuffer, m_filterBufferSize - 1, ImGuiInputTextFlags_CallbackAlways, onFilterTextEntered, &m_filterText);


	m_currLevel = (MessageLevel)m_currentErrorLevelLocation;
}

std::string DebugConsole::LevelEmumToString(MessageLevel level)
{
	std::string str;
	switch (level)
	{
	case DebugConsole::General:
		str = "General";
		break;
	case DebugConsole::Warning:
		str = "Warning";
		break;
	case DebugConsole::Error:
		str = "Error";
		break;
	case DebugConsole::Critical:
		str = "Critical";
		break;
	case DebugConsole::All:
		str = "None";
		break;
	default:
		break;
	}

	return str;
}

int DebugConsole::onFilterTextEntered(ImGuiTextEditCallbackData* data)
{
	std::string& filterMessage = *(std::string*)data->UserData;
	filterMessage = data->Buf;
	
	return 1;
}

bool DebugConsole::CheckForFilterMatch(MessageInfo& msg)
{
	if (FindStringIC(msg.category, m_filterText)) return true;
	if (FindStringIC(msg.string, m_filterText)) return true;

	return false;
}

bool DebugConsole::FindStringIC(const std::string & strHaystack, const std::string & strNeedle)
{
	auto it = std::search(
		strHaystack.begin(), strHaystack.end(),
		strNeedle.begin(), strNeedle.end(),
		[](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }
	);
	return (it != strHaystack.end());
}

void DebugConsole::ToggleWindow()
{
	m_bDebugWindowVisible = !m_bDebugWindowVisible;
}

