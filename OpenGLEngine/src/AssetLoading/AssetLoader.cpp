#include "AssetLoading/AssetLoader.h"
namespace GLEngine
{

	AssetLoader::AssetLoader()
		: m_assetPath("")
	{

	}

	AssetLoader::~AssetLoader()
	{
		assert(m_trackedAssets.empty() && "If you hit this, you have a memory leak!");
	}

	void AssetLoader::SetAssetPath(std::string assetPath)
	{
		m_assetPath = assetPath;
	}

	void AssetLoader::RemoveTrackedAsset(IAssetTracker* asset)
	{
		m_trackedAssets.erase(asset->GetAssetID());
		delete asset;
	}
};

