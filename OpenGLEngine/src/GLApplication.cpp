#include "GLApplication.h"
#include "aie/Gizmos.h"
#include <assert.h>

#include "Cameras/BaseCamera.h"

#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"

#include "Logging/DebugConsole.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;
namespace GLEngine
{

	GLApplication::GLApplication(std::string appName, unsigned int uiWidth, unsigned int uiHeight, BaseCamera* pCamera)
		: m_strAppName(appName)
		, m_pCamera(pCamera)
		, m_uiScreenWidth(uiWidth)
		, m_uiScreenHeight(uiHeight)
		, m_fDeltaTime(0.0f)
		, m_bRunning(false)
		, m_clearColour(0.25f, 0.25f, 0.25f)
	{
		m_fTotalRunTime = glfwGetTime();

	}

	GLApplication::~GLApplication()
	{

	}

	bool GLApplication::InitializeOpenGL()
	{
		if (glfwInit() == false)
			return false;

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		m_pWindow = glfwCreateWindow(m_uiScreenWidth, m_uiScreenHeight, m_strAppName.c_str(), nullptr, nullptr);

		if (m_pWindow == nullptr)
		{
			glfwTerminate();
			return false;
		}

		glfwMakeContextCurrent(m_pWindow);

		if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
		{
			glfwDestroyWindow(m_pWindow);
			glfwTerminate();
			return false;
		}

		glEnable(GL_DEPTH_TEST); // enables the depth buffer
		glEnable(GL_FRONT_AND_BACK);

		return true;
	}

	void GLApplication::Run()
	{
		InitializeOpenGL();

		Gizmos::create();

		//Initialise openGL and GLFW first..

		//Then initialise ImGui
		ImGui_ImplGlfwGL3_Init(m_pWindow, true);

		ImGuiIO& io = ImGui::GetIO();
		io.DisplaySize.x = (float)m_uiScreenWidth;
		io.DisplaySize.y = (float)m_uiScreenHeight;

		DebugConsole::Create();

		Startup();

		m_bRunning = true;
		while (!glfwWindowShouldClose(m_pWindow) && m_bRunning)
		{
			double currTime = glfwGetTime();
			m_fDeltaTime = currTime - m_fTotalRunTime;
			m_fTotalRunTime = currTime;

			//Clear the OpenGL backbuffer in preparation for rendering
			glClearColor(m_clearColour.r, m_clearColour.g, m_clearColour.b, 1);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			//Clear Gizmos
			Gizmos::clear();
			//Clear ImGui
			ImGui_ImplGlfwGL3_NewFrame();

			m_bRunning = Update(m_fDeltaTime);

			static bool bKeyPressed = false;
			if (!bKeyPressed && glfwGetKey(m_pWindow, '`'))
			{
				DebugConsole::Get()->ToggleWindow();
				bKeyPressed = true;
			}
			else if (!glfwGetKey(m_pWindow, '`')) { bKeyPressed = false;  }


			if (m_pCamera)
			{
				Render();
				Gizmos::draw(m_pCamera->GetProjection() * m_pCamera->GetView());
			}


			DebugConsole::Get()->Render();
			ImGui::Render();

			glfwSwapBuffers(m_pWindow);
			glfwPollEvents();
		}

		Shutdown();


		DebugConsole::Shutdown();

		Gizmos::destroy();

		ImGui_ImplGlfwGL3_Shutdown();
		glfwDestroyWindow(m_pWindow);
		glfwTerminate();

	}

	void GLApplication::DrawGizmoGrid()
	{

		Gizmos::addTransform(glm::mat4(1));

		vec4 white(1);
		vec4 black(0, 0, 0, 1);

		for (int i = 0; i < 21; ++i)
		{
			Gizmos::addLine(vec3(-10 + i, 0, 10),
				vec3(-10 + i, 0, -10),
				i == 10 ? white : black);

			Gizmos::addLine(vec3(10, 0, -10 + i),
				vec3(-10, 0, -10 + i),
				i == 10 ? white : black);
		}
	}

};
