#include "Resources/Mesh.h"
#include "AssetLoading/AssetLoader.h"

#include <iostream>
#include <string>

#include "tiny_obj_loader.h"

#ifdef FBX_SUPPORTED
#include "FBXFile.h"
#endif

#include <memory>
#include "Cameras/BaseCamera.h"

#include "Resources/Material.h"

#include "Logging/DebugConsole.h"

namespace GLEngine
{
	Mesh::Mesh(IAssetTracker* pAssetTracker, std::string& strFilePath)
		: Asset(pAssetTracker, strFilePath)
		, m_pRenderObject(nullptr)
		, m_pMeshShader(nullptr)
		, m_pMeshMaterial(nullptr)
		, m_bValidated(false)
	{
		//If asset tracker is null, we will be a internal mesh and so won't be loaded from disk
		if (pAssetTracker == nullptr) return;

		std::cout << strFilePath.find_last_of(".") + 1 << std::endl;

		size_t extLoc = strFilePath.find_last_of(".") + 1;
		assert(extLoc != 0 && "Need to include file extension");

		std::string strExt = strFilePath.substr(strFilePath.find_last_of(".") + 1);

		if (strExt == "obj")
		{
			LoadFromOBJ(strFilePath);
		}
		else if (strExt == "fbx")
		{
			LoadFromFBX(strFilePath);
		}
		else
		{
			assert(false && "Filetype not supported!");
		}
	}


	Mesh::~Mesh()
	{
		if (m_pMeshShader)
			m_pMeshShader->Unload();

		if (m_pMeshMaterial)
			m_pMeshMaterial->Unload();

		for (unsigned int i = 0; i < m_internalMeshes.size(); ++i)
		{
			delete m_internalMeshes[i];
		}

		delete m_pRenderObject;
	}

	void Mesh::onBind()
	{
		if (!m_bValidated) ValidateMesh();

		if(m_pMeshShader)		m_pMeshShader->Bind();
		if(m_pMeshMaterial)		m_pMeshMaterial->Bind();
		m_pRenderObject->Bind();
	}

	void Mesh::onUnbind()
	{
		if (m_pMeshShader)		m_pRenderObject->Unbind();
		if (m_pMeshMaterial)	m_pMeshMaterial->Unbind();
		m_pMeshShader->Unbind();
	}

	void Mesh::Render(BaseCamera* pCamera, bool bBindShaderUniforms)
	{
		if (!m_bInitialized) return;

		if (bBindShaderUniforms)
		{
			m_pMeshShader->SetProjectionUniform(pCamera->GetProjection());
			m_pMeshShader->SetViewUniform(pCamera->GetView());
			m_pMeshShader->SetProjectionViewUniform(pCamera->GetProjectionView());
			m_pMeshShader->SetProjectionViewModelUniform(pCamera->GetProjectionView());
		}

		Bind();
			m_pRenderObject->Render();
		Unbind();

		//Also need to render each internal mesh - they will be using the same shader
		for (unsigned int i = 0; i < m_internalMeshes.size(); ++i)
		{
			Mesh* pMesh = m_internalMeshes[i];
			pMesh->Render(pCamera, false);
		}
	}

	void Mesh::SetShader(std::string strShaderPath)
	{
		m_pMeshShader = m_pOwningAssetTracker->GetAssetLoader()->LoadAsset<Shader>(strShaderPath);
		for (unsigned int i = 0; i < m_internalMeshes.size(); ++i)
		{
			m_internalMeshes[i]->SetShader(strShaderPath);
		}
	}

	void Mesh::LoadFromOBJ(std::string path)
	{
		std::string modelDir = GetPathDirectory(path);

		assert(m_pRenderObject == nullptr && "Already loaded an object in this Mesh");

		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;
		std::string err;
		bool bLoaded = tinyobj::LoadObj(shapes, materials, err, path.c_str(), modelDir.c_str());
		
		if (!bLoaded)
		{
			DebugConsole::Get()->Message(DebugConsole::Critical, "Mesh", std::string("Failed to load mesh file: ") + path);
			return;
		}
		if (!err.empty())
		{
			DebugConsole::Get()->Message(DebugConsole::Warning, "Mesh", std::string("OBJ loaded with warning: ") + err);
		}

		tinyobj::shape_t* pMesh = &shapes[0];
		assert(pMesh && "Require at least one mesh in your OBJ");
		
		BuildRenderDataFromLoaderNode(&m_pRenderObject, pMesh);
		if (pMesh->mesh.material_ids[0] >= 0)
		{
			BuildMaterialFromLoaderNode(&m_pMeshMaterial, &materials[pMesh->mesh.material_ids[0]], modelDir);
		}

		for (unsigned int i = 1; i < shapes.size(); ++i)
		{

			DebugConsole::Get()->Message(DebugConsole::General, "Mesh", std::string("\tLoading mesh part: ") + shapes[i].name);

			Mesh* pNewMesh = new Mesh(nullptr, m_pOwningAssetTracker->GetAssetID() + std::to_string(i));
			BuildRenderDataFromLoaderNode(&(pNewMesh->m_pRenderObject), &shapes[i]);
			
			if (shapes[i].mesh.material_ids[0] >= 0)
			{
				BuildMaterialFromLoaderNode(&(pNewMesh->m_pMeshMaterial), &materials[shapes[i].mesh.material_ids[0]], modelDir);
			}

			if (m_pMeshShader) pNewMesh->SetShader(m_pMeshShader->GetAssetID());

			pNewMesh->m_pOwningAssetTracker = m_pOwningAssetTracker;
			pNewMesh->m_bInitialized = true;
			m_internalMeshes.push_back(pNewMesh);
		}

		m_bInitialized = true;
		DebugConsole::Get()->Message(DebugConsole::General, "Mesh", std::string("Finished loading mesh: ") + path);
	}

	void Mesh::LoadFromFBX(std::string path)
	{

#ifndef FBX_SUPPORTED
		assert(false && "FBX support was not compiled into this version of the engine.  Please enable before loading FBX files");
#else
		assert(m_pRenderObject == nullptr && "Already loaded an object in this Mesh");


		auto fbx = std::make_unique<FBXFile>();
		bool bLoaded = fbx->load(path.c_str()); 
		if (!bLoaded)
		{
			DebugConsole::Get()->Message(DebugConsole::Critical, "Mesh", std::string("Failed to load mesh file: ") + path);
			return;
		}
		
		FBXMeshNode* pMesh = fbx->getMeshByIndex(0);
		assert(pMesh && "Require at least one mesh in your FBX");

		BuildRenderDataFromLoaderNode(&m_pRenderObject, pMesh);
		BuildMaterialFromLoaderNode(&m_pMeshMaterial, pMesh->m_materials[0]);

		for (unsigned int i = 1; i < fbx->getMeshCount(); ++i)
		{

			DebugConsole::Get()->Message(DebugConsole::General, "Mesh", std::string("\tLoading mesh part: ") + fbx->getMeshByIndex(i)->m_name);

			Mesh* pNewMesh = new Mesh(nullptr, m_pOwningAssetTracker->GetAssetID() + std::to_string(i));
			BuildRenderDataFromLoaderNode(&(pNewMesh->m_pRenderObject), fbx->getMeshByIndex(i));
			BuildMaterialFromLoaderNode(&(pNewMesh->m_pMeshMaterial), fbx->getMeshByIndex(i)->m_materials[0]);

			if(m_pMeshShader) pNewMesh->SetShader(m_pMeshShader->GetAssetID());
			
			pNewMesh->m_pOwningAssetTracker = m_pOwningAssetTracker;
			pNewMesh->m_bInitialized = true;
			m_internalMeshes.push_back(pNewMesh);
		}



		m_bInitialized = true;
		DebugConsole::Get()->Message(DebugConsole::General, "Mesh", std::string("Finished loading mesh: ") + path);
#endif
	}


#ifdef FBX_SUPPORTED
	void Mesh::BuildRenderDataFromLoaderNode(RenderData** pRenderData, FBXMeshNode* pMesh)
	{
		assert(pMesh);
		assert(pRenderData);

		*pRenderData = new RenderData();
		(*pRenderData)->GenerateBuffers(RenderData::Buffers::ALL);

		(*pRenderData)->Bind();

		glBufferData(GL_ARRAY_BUFFER,
			pMesh->m_vertices.size() * sizeof(FBXVertex),
			pMesh->m_vertices.data(), GL_STATIC_DRAW);

		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			pMesh->m_indices.size() * sizeof(unsigned int),
			pMesh->m_indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //position
		glEnableVertexAttribArray(1); //normal data
		glEnableVertexAttribArray(2); //colour data
		glEnableVertexAttribArray(3); //texcoord1 data
		glEnableVertexAttribArray(4); //texcoord2 data
		glEnableVertexAttribArray(5); //tangent data
		glEnableVertexAttribArray(6); //binormal data
		glEnableVertexAttribArray(7); //indicies data
		glEnableVertexAttribArray(8); //weights data

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::PositionOffset);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::NormalOffset);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::ColourOffset);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::TexCoord1Offset);
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::TexCoord2Offset);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::TangentOffset);
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::BiNormalOffset);
		glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::IndicesOffset);
		glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::Offsets::WeightsOffset);

		(*pRenderData)->SetIndexCount(pMesh->m_indices.size());

		(*pRenderData)->Unbind();
	}
#endif

	void Mesh::BuildRenderDataFromLoaderNode(RenderData** pRenderData, tinyobj::shape_t* pMesh)
	{
		assert(pMesh);
		assert(pRenderData);

		struct OBJVertex
		{
			glm::vec4 position;
			glm::vec4 normal;
			glm::vec2 texCoords;
		};

		std::vector<OBJVertex> verts;
		unsigned int uiCount = 0;
		unsigned int uiUVCount = 0;

		auto& positions = pMesh->mesh.positions;
		auto& normals = pMesh->mesh.normals;
		auto& texCoords = pMesh->mesh.texcoords;

		verts.reserve(positions.size() / 3);
		while (uiCount < positions.size())
		{
			OBJVertex v;
			v.position = glm::vec4(positions[uiCount], positions[uiCount + 1], positions[uiCount + 2], 1.0f);
			if (uiCount + 2 < normals.size())
				v.normal = glm::vec4(normals[uiCount], normals[uiCount + 1], normals[uiCount + 2], 0.0f);

			if (uiUVCount + 2 < texCoords.size())
				v.texCoords = glm::vec2(texCoords[uiUVCount], -texCoords[uiUVCount + 1]);

			verts.push_back(v);

			uiCount += 3;
			uiUVCount += 2;
		}

		(*pRenderData) = new RenderData();
		(*pRenderData)->GenerateBuffers(RenderData::Buffers::ALL);

		(*pRenderData)->Bind();

		glBufferData(GL_ARRAY_BUFFER,
			verts.size() * sizeof(OBJVertex),
			verts.data(), GL_STATIC_DRAW);

		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			pMesh->mesh.indices.size() * sizeof(unsigned int),
			pMesh->mesh.indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //position
		glEnableVertexAttribArray(1); //normal data
		glEnableVertexAttribArray(3); //texCoords data
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)offsetof(OBJVertex, position));
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)offsetof(OBJVertex, normal));
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)offsetof(OBJVertex, texCoords));

		(*pRenderData)->SetIndexCount(pMesh->mesh.indices.size());

		(*pRenderData)->Unbind();
	}

	void Mesh::ValidateMesh()
	{
		bool bSuccessfullyValidated = true;
		std::string validationFailReason;

		if (m_pMeshMaterial == nullptr)
		{
			bSuccessfullyValidated = false;
			validationFailReason += "\tNo Material!\n";
		}
		else if (m_pMeshShader == nullptr) 
		{
			bSuccessfullyValidated = false;
			validationFailReason += "\tNo Shader!\n";
		}
		else
		{
			if(m_pMeshShader->HasUniform(Shader::SupportedShaderUniforms::DiffuseTexture)
				&& !m_pMeshMaterial->HasTexture(Material::DiffuseTexture))
			{
				bSuccessfullyValidated = false;
				validationFailReason += "\tShader requires Diffuse texture, but Material has none!\n";
			}
			if (m_pMeshShader->HasUniform(Shader::SupportedShaderUniforms::AmbientTexture)
				&& !m_pMeshMaterial->HasTexture(Material::AmbientTexture))
			{
				bSuccessfullyValidated = false;
				validationFailReason += "\tShader requires Ambient texture, but Material has none!\n";
			}
			if (m_pMeshShader->HasUniform(Shader::SupportedShaderUniforms::GlowTexture)
				&& !m_pMeshMaterial->HasTexture(Material::GlowTexture))
			{
				bSuccessfullyValidated = false;
				validationFailReason += "\tShader requires Glow texture, but Material has none!\n";
			}
			if (m_pMeshShader->HasUniform(Shader::SupportedShaderUniforms::SpecularTexture)
				&& !m_pMeshMaterial->HasTexture(Material::SpecularTexture))
			{
				bSuccessfullyValidated = false;
				validationFailReason += "\tShader requires Specular texture, but Material has none!\n";
			}
			if (m_pMeshShader->HasUniform(Shader::SupportedShaderUniforms::GlossTexture)
				&& !m_pMeshMaterial->HasTexture(Material::GlossTexture))
			{
				bSuccessfullyValidated = false;
				validationFailReason += "\tShader requires Gloss texture, but Material has none!\n";

			}
			if (m_pMeshShader->HasUniform(Shader::SupportedShaderUniforms::NormalTexture)
				&& !m_pMeshMaterial->HasTexture(Material::NormalTexture))
			{
				bSuccessfullyValidated = false;
				validationFailReason += "\tShader requires Normal texture, but Material has none!\n";
			}
			if (m_pMeshShader->HasUniform(Shader::SupportedShaderUniforms::AlphaTexture)
				&& !m_pMeshMaterial->HasTexture(Material::AlphaTexture))
			{
				bSuccessfullyValidated = false;
				validationFailReason += "\tShader requires Alpha texture, but Material has none!\n";
			}
			if (m_pMeshShader->HasUniform(Shader::SupportedShaderUniforms::DisplacementTexture)
				&& !m_pMeshMaterial->HasTexture(Material::DisplacementTexture))
			{
				bSuccessfullyValidated = false;
				validationFailReason += "\tShader requires Displacement texture, but Material has none!\n";
			}
		}

		if (!bSuccessfullyValidated)
		{
			DebugConsole::Get()->Message(DebugConsole::Warning, "Mesh", "Failed to validate mesh ("
				+ m_pOwningAssetTracker->GetAssetID() + ")\n" + validationFailReason);

		}

		m_bValidated = true;
	}

	std::string Mesh::GetPathDirectory(std::string path)
	{
		if (path.find_last_of("/") == std::string::npos) return path;

		return path.substr(0, path.find_last_of("/") + 1);
	}

};