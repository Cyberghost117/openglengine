#include "Resources/Material.h"
#include "AssetLoading/AssetLoader.h"

#ifdef FBX_SUPPORTED
	#include "FBXFile.h"
#endif
#include "tiny_obj_loader.h"

#include "Logging/DebugConsole.h"

namespace GLEngine
{

Material::Material(IAssetTracker* pAssetTracker, std::string& strMaterialName)
	: Asset(pAssetTracker, std::string("Material#") + strMaterialName)
{
	memset(m_pTextures, 0, sizeof(Texture*) * TextureSlots::TextureTypes_Count);
}

Material::~Material()
{
	for (int i = 0; i < TextureSlots::TextureTypes_Count; ++i)
	{
		if (m_pTextures[i])
		{
			m_pTextures[i]->Unload();
		}
	}
}

void Material::onBind()
{
	for (int i = 0; i < TextureSlots::TextureTypes_Count; ++i)
	{
		if (m_pTextures[i])	m_pTextures[i]->Bind();
	}
}

void GLEngine::Material::onUnbind()
{
	for (int i = 0; i < TextureSlots::TextureTypes_Count; ++i)
	{
		if (m_pTextures[i])	m_pTextures[i]->Unbind();
	}
}


#ifdef FBX_SUPPORTED

void Material::LoadIfExists(TextureSlots slot, FBXMaterial* pMaterial, unsigned int uiTextureType, std::string strAdditionalPath)
{
	if (pMaterial->textures[uiTextureType])
	{
		m_pTextures[slot] = m_pOwningAssetTracker->GetAssetLoader()->LoadAsset<Texture>(strAdditionalPath + pMaterial->textures[uiTextureType]->path, false);
		m_pTextures[slot]->SetTextureSlot(slot);
	}
}

#endif

void Material::LoadIfExists(TextureSlots slot, tinyobj::material_t* pMaterial, unsigned int uiTextureType, std::string strAdditionalPath)
{
	std::string texName = "";
	switch (uiTextureType)
	{
	case DiffuseTexture:
		texName = pMaterial->diffuse_texname;
		break;
	case AmbientTexture:
		texName = pMaterial->ambient_texname;
		break;
	case GlossTexture:
		texName = pMaterial->specular_highlight_texname;
		break;
	case SpecularTexture:
		texName = pMaterial->specular_texname;
		break;
	case NormalTexture:
		texName = pMaterial->bump_texname;
		break;
	case AlphaTexture:
		texName = pMaterial->alpha_texname;
		break;
	case DisplacementTexture:
		texName = pMaterial->displacement_texname;
		break;
	case TextureTypes_Count:
		break;
	}

	if (texName != "")
	{
		m_pTextures[slot] = m_pOwningAssetTracker->GetAssetLoader()->LoadAsset<Texture>(strAdditionalPath + pMaterial->diffuse_texname, false);
		m_pTextures[slot]->SetTextureSlot(slot);
	}

}




}

