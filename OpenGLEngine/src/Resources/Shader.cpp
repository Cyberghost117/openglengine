#include "Resources/Shader.h"
#include "Resources/Material.h"

#include <fstream>
#include <assert.h>
#include <sstream>

#include "Logging/DebugConsole.h"

namespace GLEngine
{

	Shader::Shader(IAssetTracker* pAssetTracker, std::string& strFilePath) : Asset(pAssetTracker, strFilePath), m_shaderID(-1)
	{
		LoadShaderProgramFromFile(strFilePath);
	}

	Shader::~Shader()
	{

	}

	void Shader::onBind()
	{
		glUseProgram(m_shaderID);
	}

	void Shader::onUnbind()
	{
		glUseProgram(0);
	}

	void Shader::SetProjectionUniform(const glm::mat4& mat)
	{
		bool b = BindIfNeeded();
		glUniformMatrix4fv(m_shaderUniforms[SupportedShaderUniforms::Projection], 1, GL_FALSE, glm::value_ptr(mat));
		UnbindIfNeeded(b);
	}

	void Shader::SetViewUniform(const glm::mat4& mat)
	{
		bool b = BindIfNeeded();
		glUniformMatrix4fv(m_shaderUniforms[SupportedShaderUniforms::View], 1, GL_FALSE, glm::value_ptr(mat));
		UnbindIfNeeded(b);
	}

	void Shader::SetModelUniform(const glm::mat4& mat)
	{
		bool b = BindIfNeeded();
		glUniformMatrix4fv(m_shaderUniforms[SupportedShaderUniforms::Model], 1, GL_FALSE, glm::value_ptr(mat));
		UnbindIfNeeded(b);
	}

	void Shader::SetProjectionViewUniform(const glm::mat4& mat)
	{
		bool b = BindIfNeeded();
		glUniformMatrix4fv(m_shaderUniforms[SupportedShaderUniforms::ProjectionView], 1, GL_FALSE, glm::value_ptr(mat));
		UnbindIfNeeded(b);
	}

	void Shader::SetProjectionViewModelUniform(const glm::mat4& mat)
	{
		bool b = BindIfNeeded();
		glUniformMatrix4fv(m_shaderUniforms[SupportedShaderUniforms::ProjectionViewModel], 1, GL_FALSE, glm::value_ptr(mat));
		UnbindIfNeeded(b);
	}


	void Shader::SetUniform(int uniformID, float value)
	{
		bool b = BindIfNeeded();
		glUniform1f(uniformID, value);
		UnbindIfNeeded(b);
	}

	void Shader::SetUniform(int uniformID, int value)
	{
		bool b = BindIfNeeded();
		glUniform1i(uniformID, value);
		UnbindIfNeeded(b);
	}

	void Shader::SetUniform(int uniformID, glm::vec2 value)
	{
		bool b = BindIfNeeded();
		glUniform2fv(uniformID, 1, glm::value_ptr(value));
		UnbindIfNeeded(b);
	}

	void Shader::SetUniform(int uniformID, glm::vec3 value)
	{
		bool b = BindIfNeeded();
		glUniform3fv(uniformID, 1, glm::value_ptr(value));
		UnbindIfNeeded(b);
	}

	void Shader::SetUniform(int uniformID, glm::vec4 value)
	{

		bool b = BindIfNeeded();
		glUniform4fv(uniformID, 1, glm::value_ptr(value));
		UnbindIfNeeded(b);
	}

	void Shader::SetUniform(int uniformID, glm::mat4 value)
	{
		bool b = BindIfNeeded();
		glUniformMatrix4fv(uniformID, 1, GL_FALSE, glm::value_ptr(value));
		UnbindIfNeeded(b);
	}

	void Shader::LoadShaderProgramFromFile(std::string& strPath)
	{
		assert(m_shaderID < 0 && "Already have loaded a shader");

		std::ifstream file;
		file.open(strPath, std::ifstream::in);

		if (!file.good())
		{
			DebugConsole::Get()->Message(DebugConsole::Critical, "Shader", "Failed to load shader descriptor: " + strPath);
			return;
		}

		std::string fullFilePath = strPath.substr(0, strPath.find_last_of('/') + 1);

		int iVertShader = 0;
		int iFragShader = 0;

		while (file.good())
		{
			std::string strLine;
			std::getline(file, strLine);
			if (strLine.substr(0, 4) == "vert")
			{
				iVertShader = LoadShader(GL_VERTEX_SHADER, fullFilePath + strLine.substr(5));
			}

			if (strLine.substr(0, 4) == "frag")
			{
				iFragShader = LoadShader(GL_FRAGMENT_SHADER, fullFilePath + strLine.substr(5));
			}
		}

		m_shaderID = glCreateProgram();
		glAttachShader(m_shaderID, iVertShader);
		glAttachShader(m_shaderID, iFragShader);
		
		SetupAttributeBindings();

		glLinkProgram(m_shaderID);

		glDeleteShader(iFragShader);
		glDeleteShader(iVertShader);

		//Error Checking
		int success = GL_FALSE;
		glGetProgramiv(m_shaderID, GL_LINK_STATUS, &success);
		if (success == GL_FALSE) {
			int infoLogLength = 0;
			glGetProgramiv(m_shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
			char* infoLog = new char[infoLogLength];
			glGetProgramInfoLog(m_shaderID, infoLogLength, 0, infoLog);
			
			DebugConsole::Get()->Message(DebugConsole::Error, "Shader",
				std::string("Failed to link shader program (") + strPath + ")\n" + infoLog);

			delete[] infoLog;

			return;
		}
	

		m_bInitialized = true;

		PrepareShaderUniforms();
		SetupTextureLocations();

		DebugConsole::Get()->Message(DebugConsole::General, "Material", "Finished loading shader: " + strPath);

	}

	int Shader::LoadShader(GLenum eType, std::string& strFilePath)
	{
		unsigned int iShader = glCreateShader(eType);

		std::ifstream file;
		file.open(strFilePath);
		if (!file.good())
		{
			DebugConsole::Get()->Message(DebugConsole::Critical, "Shader", "Failed to find shader file: " + strFilePath);
			return -1;
		}

		std::stringstream ss;
		ss << file.rdbuf();
		file.close();


		std::string shaderCode = ss.str();
		const char* pCode = shaderCode.c_str();
		glShaderSource(iShader, 1, (const char**)&pCode, 0);
		glCompileShader(iShader);

		int success = GL_FALSE;
		glGetShaderiv(iShader, GL_COMPILE_STATUS, &success);
		if (success == GL_FALSE) {
			int infoLogLength = 0;
			glGetShaderiv(iShader, GL_INFO_LOG_LENGTH, &infoLogLength);
			char* infoLog = new char[infoLogLength];
			glGetShaderInfoLog(iShader, infoLogLength, 0, infoLog);

			DebugConsole::Get()->Message(DebugConsole::Error, "Shader",
				std::string("Failed to compile shader program (") + strFilePath + ")\n" + infoLog);

			delete[] infoLog;

			return -1;
		}


		return iShader;
	}

	void Shader::PrepareShaderUniforms()
	{
		Bind();
		m_shaderUniforms[SupportedShaderUniforms::Projection] = glGetUniformLocation(m_shaderID, "Projection");
		m_shaderUniforms[SupportedShaderUniforms::View] = glGetUniformLocation(m_shaderID, "View");
		m_shaderUniforms[SupportedShaderUniforms::Model] = glGetUniformLocation(m_shaderID, "Model");
		m_shaderUniforms[SupportedShaderUniforms::ProjectionView] = glGetUniformLocation(m_shaderID, "ProjectionView");
		m_shaderUniforms[SupportedShaderUniforms::ProjectionViewModel] = glGetUniformLocation(m_shaderID, "ProjectionViewModel");
		Unbind();
	}

	void Shader::SetupAttributeBindings()
	{
	
		glBindAttribLocation(m_shaderID, 0, "Position");
		glBindAttribLocation(m_shaderID, 1, "Normal");
		glBindAttribLocation(m_shaderID, 2, "Colour");
		glBindAttribLocation(m_shaderID, 3, "TexCoords");
		glBindAttribLocation(m_shaderID, 4, "TexCoords2");
		glBindAttribLocation(m_shaderID, 5, "Tangent");
		glBindAttribLocation(m_shaderID, 6, "Binormal");
		glBindAttribLocation(m_shaderID, 7, "Indicies");
		glBindAttribLocation(m_shaderID, 8, "Weights");
	}

	void Shader::SetupTextureLocations()
	{
		Bind();

		m_shaderUniforms[SupportedShaderUniforms::DiffuseTexture] = glGetUniformLocation(m_shaderID, "DiffuseTexture");
		m_shaderUniforms[SupportedShaderUniforms::AmbientTexture] = glGetUniformLocation(m_shaderID, "AmbientTexture");
		m_shaderUniforms[SupportedShaderUniforms::GlowTexture] = glGetUniformLocation(m_shaderID, "GlowTexture");
		m_shaderUniforms[SupportedShaderUniforms::SpecularTexture] = glGetUniformLocation(m_shaderID, "SpecularTexture");
		m_shaderUniforms[SupportedShaderUniforms::GlossTexture] = glGetUniformLocation(m_shaderID, "GlossTexture");
		m_shaderUniforms[SupportedShaderUniforms::NormalTexture] = glGetUniformLocation(m_shaderID, "NormalTexture");
		m_shaderUniforms[SupportedShaderUniforms::AlphaTexture] = glGetUniformLocation(m_shaderID, "AlphaTexture");
		m_shaderUniforms[SupportedShaderUniforms::DisplacementTexture] = glGetUniformLocation(m_shaderID, "DisplacementTexture");
		
		
		glUniform1i(m_shaderUniforms[SupportedShaderUniforms::DiffuseTexture],	Material::DiffuseTexture);
		glUniform1i(m_shaderUniforms[SupportedShaderUniforms::AmbientTexture],	Material::AmbientTexture);
		glUniform1i(m_shaderUniforms[SupportedShaderUniforms::GlowTexture],		Material::GlowTexture);
		glUniform1i(m_shaderUniforms[SupportedShaderUniforms::SpecularTexture], Material::SpecularTexture);
		glUniform1i(m_shaderUniforms[SupportedShaderUniforms::GlossTexture],	Material::GlossTexture);
		glUniform1i(m_shaderUniforms[SupportedShaderUniforms::NormalTexture],	Material::NormalTexture);
		glUniform1i(m_shaderUniforms[SupportedShaderUniforms::AlphaTexture],	Material::AlphaTexture);
		glUniform1i(m_shaderUniforms[SupportedShaderUniforms::DisplacementTexture], Material::DisplacementTexture);

		Unbind();
	}

	

};