#include "Resources/Asset.h"
#include "AssetLoading/AssetLoader.h"

#include "AssetLoading/AssetTracker.h"
namespace GLEngine
{

	Asset::Asset(IAssetTracker* pAssetTracker, std::string& strFilePath) 
		: m_pOwningAssetTracker(pAssetTracker)
		, m_strFilePath(strFilePath)
		, m_bInitialized(false)
		, m_bCurrentBound(false)
	{

	}

	void Asset::Bind()
	{
		if (m_bInitialized)
		{
			onBind();
			m_bCurrentBound = true;
		}
	}

	void Asset::Unbind()
	{
		if (m_bInitialized)
		{
			m_bCurrentBound = false;
			onUnbind();
		}
	}

	void Asset::Unload()
	{
		m_pOwningAssetTracker->Unload();
	}

	std::string Asset::GetAssetID() const
	{
		return m_pOwningAssetTracker->GetAssetID();
	}

	bool Asset::BindIfNeeded()
	{
		if (!m_bCurrentBound)
		{
			Bind();
			return true;
		}
		else
		{
			return false;
		}
	}

	void Asset::UnbindIfNeeded(bool b)
	{
		if (b)
		{
			Unbind();
		}
	}

};