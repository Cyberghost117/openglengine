#include "Rendering/RenderData.h"

#include <iostream>

namespace GLEngine
{

	RenderData::RenderData()
		: m_vao(0)
		, m_vbo(0)
		, m_ibo(0)
	{
	}

	RenderData::~RenderData()
	{
		DestroyBuffers(Buffers::ALL);
	}

	RenderData::RenderData(const RenderData& a_other)
	{
		m_vao = a_other.m_vao;
		m_vbo = a_other.m_vbo;
		m_ibo = a_other.m_ibo;
	}

	RenderData::RenderData(RenderData&& a_other)
	{

		m_vao = a_other.m_vao;
		m_vbo = a_other.m_vbo;
		m_ibo = a_other.m_ibo;

		a_other.m_vao = 0;
		a_other.m_vbo = 0;
		a_other.m_ibo = 0;
	}

	void RenderData::GenerateBuffers(unsigned char a_uiBuffers)
	{
		//If any buffers exist of the type we're creating,
		//destroy them so we don't leak memory
		DestroyBuffers(a_uiBuffers);

		if (a_uiBuffers & Buffers::VAO)
		{
			glGenVertexArrays(1, &m_vao);
			glBindVertexArray(m_vao);
		}

		if (a_uiBuffers & Buffers::VBO)
		{
			glGenBuffers(1, &m_vbo);
			if (a_uiBuffers & Buffers::VAO)
			{
				glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
			}
		}

		if (a_uiBuffers & Buffers::IBO)
		{
			glGenBuffers(1, &m_ibo);
			if (a_uiBuffers & Buffers::IBO) glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
		}

		if (a_uiBuffers & Buffers::VAO) glBindVertexArray(0);
	}

	void RenderData::DestroyBuffers(unsigned char a_uiBuffers)
	{
		if (a_uiBuffers & Buffers::VAO && m_vao > 0)
		{
			glDeleteVertexArrays(1, &m_vao);
		}
		if (a_uiBuffers & Buffers::VBO && m_vbo > 0)
		{
			glDeleteBuffers(1, &m_vbo);
		}
		if (a_uiBuffers & Buffers::IBO && m_ibo > 0)
		{
			glDeleteBuffers(1, &m_ibo);
		}
	}

	void RenderData::CheckBuffers(unsigned char& a_uiBuffers)
	{
		a_uiBuffers = 0;
		a_uiBuffers |= (m_vao > 0) ? Buffers::VAO : 0;
		a_uiBuffers |= (m_vbo > 0) ? Buffers::VBO : 0;
		a_uiBuffers |= (m_ibo > 0) ? Buffers::IBO : 0;
	}


	void RenderData::Bind()
	{
		if (m_vao > 0)
		{
			glBindVertexArray(m_vao);
		}
		else if (m_vbo > 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
			if (m_ibo > 0) glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
		}
	}

	void RenderData::Render()
	{
		if (m_ibo)
		{
			glDrawElements(GL_TRIANGLES, m_uiIndexCount, GL_UNSIGNED_INT, 0);
		}
	}

	void RenderData::Unbind()
	{
		if (m_vao > 0)
		{
			glBindVertexArray(0);
		}
		else if (m_vbo > 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			if (m_ibo > 0) glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}
	}


};