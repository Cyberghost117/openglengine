#pragma once
    
#include <gl_core_4_4.h>    

namespace GLEngine
{

	class RenderData
	{
	public:
		RenderData();
		~RenderData();

		//Can't copy render data
		RenderData(const RenderData& other);

		//But can move it
		RenderData(RenderData&& other);

		enum Buffers : unsigned char
		{
			VAO = 1,
			VBO = 2,
			IBO = 4,
			ALL = 7
		};

		void GenerateBuffers(unsigned char uiBuffers);
		void DestroyBuffers(unsigned char  uiBuffers);

		void CheckBuffers(unsigned char& uiBuffers);

		GLuint GetVAO() const { return m_vao; }
		GLuint GetVBO() const { return m_vbo; }
		GLuint GetIBO() const { return m_ibo; }

		void Bind();
		void Render();
		void Unbind();

		void SetIndexCount(unsigned int uiCount) { m_uiIndexCount = uiCount; }
		unsigned int GetIndexCount() const { return m_uiIndexCount; }
	private:
		unsigned int m_uiIndexCount;

		GLuint m_vao;
		GLuint m_vbo;
		GLuint m_ibo;
	};
};