#pragma once

#define ENGINE_VERSION 1

#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <memory>
#include <string>

//#include <vld.h>
namespace GLEngine
{
	class BaseCamera;

	class GLApplication
	{
	public:
		GLApplication(std::string appName, unsigned int uiWidth = 1280, unsigned int uiHeight = 720, BaseCamera* m_pCamera = nullptr);
		~GLApplication();

		virtual bool Startup() = 0;
		virtual void Shutdown() = 0;

		virtual bool Update(double dt) = 0;
		virtual void Render() = 0;
		void Run();

		BaseCamera* GetCamera() { return m_pCamera; }

		void DrawGizmoGrid();
	protected:
		unsigned int GetScreenWidth() const { return m_uiScreenWidth; }
		unsigned int GetScreenHeight() const { return m_uiScreenHeight; }

		std::string m_strAppName;
		GLFWwindow* m_pWindow;

		BaseCamera* m_pCamera;

		glm::vec3 m_clearColour;
	private:
		//You shall not call this!
		GLApplication() {};

		//Initialization helper functions
		bool InitializeOpenGL();

		unsigned int m_uiScreenWidth;
		unsigned int m_uiScreenHeight;

		double m_fTotalRunTime;
		double m_fDeltaTime;
		bool m_bRunning;
	};

};