#pragma once

#include <string>
#include <memory>
namespace GLEngine
{

	class AssetLoader;
	class IAssetTracker
	{

	public:
		IAssetTracker(AssetLoader* pLoader, std::string id);
		IAssetTracker(IAssetTracker&& other); //Move constructor
		IAssetTracker& operator=(IAssetTracker&& other);

		virtual ~IAssetTracker() { }

		IAssetTracker() = delete; //Must assign ID
		IAssetTracker(const IAssetTracker& other) = delete; //Can't copy
		IAssetTracker& operator=(const IAssetTracker& other) = delete; //No copying


		std::string		GetAssetID() const { return m_assetID; }
		unsigned int	GetRefCount() const { return m_refCount; }

		unsigned int GetRefCount() { return m_refCount; }

		virtual void Unload() = 0;


		unsigned int IncRefCount() { ++m_refCount; return m_refCount; }
		unsigned int DecRefCount() { --m_refCount; ; return m_refCount; }


		AssetLoader* GetAssetLoader() const { return m_pLoader; }
	protected:
		AssetLoader* m_pLoader;
	private:
		std::string  m_assetID;
		unsigned int m_refCount;
	};

	template<typename T>
	class AssetTracker : public IAssetTracker
	{
	public:
		AssetTracker(AssetLoader* pLoader, std::string id);
		AssetTracker(IAssetTracker&& other); //Move constructor
		AssetTracker& operator=(AssetTracker&& other);

		~AssetTracker() { }

		AssetTracker() = delete; //Must assign ID
		AssetTracker(const AssetTracker& other) = delete; //Can't copy
		AssetTracker& operator=(const AssetTracker& other) = delete; //No copying

		T* GetAsset() { return m_assetObject.get(); }

		void BindAsset(T* pAsset) {
			m_assetObject.reset(pAsset);
		}

		void Unload() override;


	protected:

	private:
		std::unique_ptr<T> m_assetObject;
	};

	template<typename T>
	AssetTracker<T>& AssetTracker<T>::operator=(AssetTracker<T>&& other)
	{
		m_assetObject = std::move(other.m_assetObject);
		return *this;
	}

	template<typename T>
	AssetTracker<T>::AssetTracker(IAssetTracker&& other)
	{
		m_assetObject = std::move(other.m_assetObject);
	}

	template<typename T>
	AssetTracker<T>::AssetTracker(AssetLoader* pLoader, std::string id)
		: IAssetTracker(pLoader, id)
		, m_assetObject(nullptr)
	{

	}

	template<typename T>
	void AssetTracker<T>::Unload()
	{
		if (DecRefCount() == 0)
		{
			m_assetObject = nullptr;
			//Calling this deletes this object!
			m_pLoader->RemoveTrackedAsset(this);
		}
	}

};