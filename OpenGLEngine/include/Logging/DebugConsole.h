#pragma once
#include <string>
#include <vector>

#include "imgui.h"

class DebugConsole
{
public:
	enum MessageLevel
	{
		General = 0,
		Warning,
		Error,
		Critical,
		All
	};

	~DebugConsole();

	static void Create();
	static void Shutdown();

	static DebugConsole* Get();

	void ShowDebugWindow();
	void HideDebugWindow();
	void ToggleWindow();

	void Render();

	void PopulateWithDebugText();

	void SetAutoPopupLevel(MessageLevel level) { m_autoPopupLevel = level; }
	void Message(MessageLevel level, std::string category, std::string msg);
private:
	struct MessageInfo
	{
		std::string category;
		std::string string;
		MessageLevel level;
		//To add:
		double time;
	};


	DebugConsole();
	void		DisplayMessage(MessageInfo& m_messageList);

	void		RenderMessageLevelButtons();


	ImVec4		GetColourBasedOnMessageLevel(MessageLevel level);
	std::string LevelEmumToString(MessageLevel level);


	static int onFilterTextEntered(ImGuiTextEditCallbackData* data);
	bool CheckForFilterMatch(MessageInfo& msg);
	bool FindStringIC(const std::string & strHaystack, const std::string & strNeedle);


	static DebugConsole* ms_pInstance;
	bool m_bDebugWindowVisible;
	std::vector<MessageInfo> m_messageList;

	MessageLevel m_currLevel;
	MessageLevel m_autoPopupLevel;


	int			m_currentErrorLevelLocation;
	const char* m_errorLevelsAsText;
	char*		m_filterBuffer;
	const int	m_filterBufferSize = 64;
	std::string m_filterText;

	bool		m_bFilteringEnabled;
};