#pragma once
#include "Resources\Asset.h"

#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <map>

namespace GLEngine
{

	class Shader : public Asset
	{
	public:
		enum SupportedShaderUniforms
		{
			/*Common required transforms*/
			Projection,
			View,
			Model,
			ProjectionView,
			ProjectionViewModel,

			/*Texture Samplers*/
			DiffuseTexture,
			AmbientTexture,
			GlowTexture,
			SpecularTexture,
			GlossTexture,
			NormalTexture,
			AlphaTexture,
			DisplacementTexture,

			/*Total number of uniforms*/
			NUM_SUPPORTED_UNIFORMS
		};

	public:
		Shader(IAssetTracker* pAssetTracker, std::string& strFilePath);
		~Shader();

		void SetProjectionUniform(const glm::mat4& mat);
		void SetViewUniform(const glm::mat4& mat);
		void SetModelUniform(const glm::mat4& mat);
		void SetProjectionViewUniform(const glm::mat4& mat);
		void SetProjectionViewModelUniform(const glm::mat4& mat);

		template<typename T>
		int SetUniform(std::string uniformName, T& value)
		{
			int iPos = -1;
			auto it = m_cachedShaderUniforms.find(uniformName);
			if (it == m_cachedShaderUniforms.end())
			{
				iPos = glGetUniformLocation(m_shaderID, uniformName.c_str());
				m_cachedShaderUniforms[uniformName] = iPos;
			}
			else
			{
				iPos = it->second;
			}

			SetUniform(iPos, value);
			return iPos;
		}

		void SetUniform(int uniformID, float value);
		void SetUniform(int uniformID, int value);
		void SetUniform(int uniformID, glm::vec2 value);
		void SetUniform(int uniformID, glm::vec3 value);
		void SetUniform(int uniformID, glm::vec4 value);
		void SetUniform(int uniformID, glm::mat4 value);

		int GetShaderID() const { return m_shaderID; }

		bool HasUniform(SupportedShaderUniforms eUniform) { return m_shaderUniforms[eUniform] != -1; }
	private:
		void onBind() override;
		void onUnbind() override;

		void	LoadShaderProgramFromFile(std::string& strPath);
		int		LoadShader(GLenum eType, std::string& strFilePath);

		void	PrepareShaderUniforms();
		void	SetupAttributeBindings();
		void	SetupTextureLocations();

		int m_shaderID;
		int m_shaderUniforms[SupportedShaderUniforms::NUM_SUPPORTED_UNIFORMS];

		std::map<std::string, int> m_cachedShaderUniforms;

	};

};