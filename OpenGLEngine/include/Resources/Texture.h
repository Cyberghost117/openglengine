#pragma once

//
#include "Asset.h"

namespace GLEngine
{

	class Texture : public Asset
	{
	public:
		Texture(IAssetTracker* pAssetTracker, std::string& strFilePath);
		~Texture();

		void SetTextureSlot(unsigned int uiSlot);
	private:
		void onBind() override;
		void onUnbind() override;

		void LoadWithSTBImage(std::string& strFilePath);

		unsigned int m_uiTextureID;
		
		//TODO: Add additional texture options
		int m_iTextureFormat;
		int m_iImageWidth;
		int m_iImageHeight;

		unsigned int m_uiTextureSlot;
	};

};