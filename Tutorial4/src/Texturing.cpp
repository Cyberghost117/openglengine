#include "Texturing.h"
#include "aie/Gizmos.h"

#include "Cameras/FlyCamera.h"
#include "Resources/Mesh.h"

#include "imgui.h"

#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"

using namespace GLEngine;

Texturing::Texturing(std::string str, unsigned int uiWidth, unsigned int uiHeight)
	: GLApplication(str, uiWidth, uiHeight)
	, m_pLoader(nullptr)
	, m_pFlyCamera(nullptr)
	, m_bDrawGizmoGrid(true)
{
}

Texturing::~Texturing()
{
	delete m_pCamera;
}

bool Texturing::Startup()
{

	glEnable(GL_BLEND);

	m_pLoader = new AssetLoader();

	m_pFlyCamera = new FlyCamera(); m_pFlyCamera->SetInputWindow(m_pWindow);
	m_pCamera = m_pFlyCamera;

	m_pCamera->SetupPerspective(glm::pi<float>() * 0.25f,
		(float)GetScreenWidth() / (float)GetScreenHeight(), 0.1f, 1000.f);

	m_pCamera->LookAt(glm::vec3(10, 10, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	m_pLoader->SetAssetPath("Data");
	m_pSpear = m_pLoader->LoadAsset<Mesh>("Models/soulspear/soulspear.fbx");
	m_pSpear->SetShader("Shaders/textured.shader");


	return true;

}

void Texturing::Shutdown()
{
	m_pSpear->Unload();

	delete m_pLoader;
}

bool Texturing::Update(double dt)
{
	m_pFlyCamera->Update(dt);

	return !(glfwGetKey(m_pWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS);
}

void Texturing::Render()
{
	if(m_bDrawGizmoGrid == true)
		DrawGizmoGrid();

	m_pSpear->GetShader()->SetModelUniform(glm::mat4(1));
	m_pSpear->Render(m_pCamera);


}